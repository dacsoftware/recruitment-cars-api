# Cześć !

## Wprowadzenie
Prosimy Cię o wykonanie prostych aplikacji

* backendowej napisanej w języku PHP 7.4 lub nowszej. 
* frontendowej napisanej w przy użyciu ReactJS, ES6 lub nowszej

Po otrzymaniu Twojego kodu będziemy chcieli ocenić Twój umiejętności i potencjał, 
żeby w następnym etapie na bazie utworzonego przez Ciebie kodu razem spróbować omówić ewnentualne problemy, 
które napotkałaś/eś podczas tworzenia kodu aplikacji. Być może zdecydujemy się na wspólną sesję programowania,
żeby dowiedzieć się jak się z Tobą pracuje.

## Cel zadania

### Warstwa API

W pliku data.json zawarliśmy ustrukturyzowane dane. Korzystając z tych danych wymagamy aplikacji wystawiającej
API z trzema metodami dostępowymi zgodnymi z REST dla encji "samochód" oraz "marka". 
API powinno zwracać dane w formacie JSON i html.

Założenia :

* nie powinieneś/aś korzystać z bibliotek / frameworków budujących API (Symfony / Laravel / FOSRestBundle / ApiPlatform itp)
* użyj bibliotek pomocniczych jeśli uznasz, że warto
* skorzystaj z paradygmatów programowania obiektowego 
* mile widziane użycie wzorców projektowych
* API wystawia metody dostępowe do **wszystkich marek**, do **wszystkich samochodów** oraz do **pojedynczego samochodu**
    ** kontrakt między warstwą frontendową a backendową stanowią zaproponowane obiekty w katalogu ValueObject
    ** wykorzystaj te obiekty, nie modyfikuj ich
* napisz testy (jednostkowe lub / i funkcjonalne)
* utrzymaj code style zgodny z założeniami standardu PSR-2

### Warstwa frontend

Zbuduj prostą aplikację przeglądarkową, w której w trzech osobnych sekcjach zaprezentujesz:

* marki
* dostępne (na podstawie pola "visibility") samochody danej marki po kliknięciu na wybraną markę 
* szczegóły auta po kliknięciu na wybrane auto

Założenia :

* możesz skorzystać z framework'a ReactJS
* użyj mniejszych bibliotek pomocniczych jeśli uznasz, że warto
* utrzymaj code style zgodny z standardjs (https://standardjs.com/)
* możesz skorzystać z typescripta
* napisz testy (jednostkowe lub / i funkcjonalne)

### Dokumentacja

W języku angielskim w pliku README.md opisz jak uruchomić projekt.

## Schemat pracy z repozytorium

Prosimy Cię o utworzenie kopii (NIE FORK'a) aktualnego repozytorium na bitbuckecie / githubie / gitlabie oraz wysłanie adresu repozytorium 
na adres: 
**developers.php@edpauto.com**.

Pracuj na gałęzi odchodzącej od mastera.
Zespół chciałby wykonać CODE review projektu, który wykonujesz. 

Prosimy Cię więc o przygotowanie Pull Request'a kiedy uznasz, że wykonałeś zadanie

i powiadomienie nas na powyższy email.
 
#### Powodzenia !


